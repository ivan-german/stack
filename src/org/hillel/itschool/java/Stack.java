package org.hillel.itschool.java;

public interface Stack {

    /**
     * Принимает строку, кладет ее в стек.
     * В стеке может быть максимум 4 элемента.
     *
     * @param arg
     *  элемент, который нужно положить в стек.
     *
     * @throws StackOverflowException
     *   если мы пытаемся положить 5ый и более
     *   элемент в стек
     */
    void push(String arg) throws StackOverflowException;

    /**
     * Возвращает последний положенный элемент и
     * удаляет его из стека.
     *
     * @return
     *   последний добавленный элемент в стек.
     *
     * @throws EmptyStackException
     *   если пытаемся достать элемент из пустого стека
     */
    String pop() throws EmptyStackException;

    /**
     * Проверяет, что стек пуст
     */
    boolean isEmpty();

    /**
     * Проверяет, что стек полон
     */
    boolean isFull();
}
